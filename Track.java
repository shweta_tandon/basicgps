import java.io.*;// importing neccessary libraries
import java.util.*;
import java.util.Scanner;
import java.lang.Math.*;
import java.util.ArrayList;
//import java.io.FileWriter;
//import java.io.Writer;

/* author: Shweta Tandon
*/

import java.io.BufferedWriter;
//package cwk1;
public class Track //making the list
{ //class

 ArrayList<Point> list;

public Track()
{ //track()
   list = new ArrayList<Point>();
} //track()

 public void add(Point point)//adds a new point to the end of the track
 { //add()

    list.add(point); //adding points to the track

  } //add()
public int size() // returns the number of points currently stored in the trac
{ //size()
   return list.size();
} //size()

Track(String newread) throws FileNotFoundException
{ //track constructor
  String read=newread;
} //track constructor

//method to read from walk.txt and adding the points
public void read(String filename) throws FileNotFoundException
{ //read()
  //BufferedReader input = new BufferedReader(new FileReader(filename));
  //FileReader reader = new FileReader(filename);
  Scanner input = new Scanner(new File(filename));

while (input.hasNextDouble()) //loop to keep getting the points till the last point of the text file
{ //while
  //return pointslength;

  double value1 = input.nextDouble(); //variables to store the longitude/latitude/elevation to the point
  double value2 = input.nextDouble();//variables to store the longitude/latitude/elevation to the point
  double value3 = input.nextDouble();//variables to store the longitude/latitude/elevation to the point

  Point newpoint=new Point(value1,value2,value3); //making the point
  add(newpoint); //adding the point to the list
} //while


input.close();
} //read()

//returns the total distance travelled in metres when moving from point to point along the entire length of the track
public double totalDistance() throws IOException
{ //totaldistance()
  double distance=0; //variable to store the distance


  for(int i = 0; i<list.size() - 1 ; i++) //loop to move iterate and calculate the distance between each point till the end of the track
  { //for
    int j=i+1; //variable to store the value of the next point so that the distance can be calculated


    distance += Point.greatCircleDistance(list.get(i), list.get(j)); //calculating the distance between two points

  } //for
  return distance; //returning the total distance of the track
} //totaldistance


//method to find out the lowest point in the track and return it
public Point lowestPoint() throws IOException
{ //lowestpoint()
  double lowestLongitude=0;  //variable to store the lowest longitude
  double lowestElevation=9999; //variable to compare and store the lowest elevation
  double lowestLatitude=0; //variable to store the lowest latitude

  for ( Point p1: list) //loop to iterate in the track/list and check for the lowest point
  { //for
    if(p1.getElevation()<lowestElevation) //checking for the lowest point
    { //if
      lowestElevation=p1.getElevation(); //storing the values of the lowest point
      lowestLongitude=p1.getLongitude(); //storing the values of the lowest point
      lowestLatitude=p1.getLatitude(); //storing the values of the lowest point
    } //if
    //Point point = new Point (lowestLongitude,lowestLatitude,lowestElevation);
  //  return point;
} //for
  Point point = new Point (lowestLongitude,lowestLatitude,lowestElevation);
  return point; //returning the point
}// lowest point

//method to return the highest point in the track
public Point highestPoint() throws IOException
{ //highest point
  double highestLongitude=0; //variable to store the highest longitude
  double highestElevation=0; //variable to store the highest longitude
  double highestLatitude=0; //variable to store the highest longitude

for(Point p1:list)//loop to iterate in the track/list and check for the highest point
{ //for
  if(p1.getElevation()>highestElevation) //checking for the highest point
  { //if
    highestElevation=p1.getElevation(); //storing the values of the highest point
    highestLongitude=p1.getLongitude(); //storing the values of the highest point
    highestLatitude=p1.getLatitude(); //storing the values of the highest point
  }//if

} //for
Point point = new Point (highestLongitude,highestLatitude,highestElevation); // storing the values in the point
return point; //returning the highest point
} //highestpoint

public Point nearestPointTo(Point target) //method to find out the nearest point to the target
{ //nearest point to


  double nearest_distance = 0; //variable to store the nearest distance
  double nearestLongitude = 0; //variable to store the nearest Longitude
  double nearestLatitude = 0; //variable to store the nearest latitude
  //  double nearestElevation=0;
  //double d =0;
  Point nearestpoint = new Point(nearestLatitude, nearestLongitude); //point to store the values of the nearest point

  for(Point n: list) //loop to iterate in the list to find out the nearest point
  { //for
    double d = Point.greatCircleDistance(target, n); // calculating and storing the distance between two points
    if (nearest_distance >= d)  //comparing the nearest distance to the distance calculated above
    { //if
      d = nearest_distance;
      nearestpoint = n; // storing the nearest points values into nearestpoint
    }//if
  } //for
  return nearestpoint; //returning the nearest point
}// nearest point to


  /*for(Point n: list)
  {
   d = Point.greatCircleDistance(n,target);
  {
    if(d>=nearest_distance)
    {
      d = nearest_distance;
      nearestLongitude= n.getLongitude();
      nearestLatitude= n.getLatitude();

    }

//Point nearest_point=new Point(nearestLongitude,nearestLatitude,nearestElevation);
  }
//Point nearest_point=new Point(nearestLongitude,nearestLatitude);

} */


//Point nearest_point=new Point(nearestLongitude,nearestLatitude);
//return nearest_point;

//method to write in the file
   public void write(String file)throws IOException
   { //write()

      java.io.File f = new java.io.File(file);
      java.io.PrintWriter output = new java.io.PrintWriter(f);


      output.print("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>");  //giving the gpx commands
      output.print("<gpx>");
      output.print("<trk>");
      output.print("<name>gps.gpx</name>");
      output.print("<trkseg>");

      String trackSeg="";
      for(Point p: list)   //going through the list
      { //for
        trackSeg +=("<trkpt lon=\"" + p.getLongitude() + "\"" + " lat=\"" + p.getLatitude() + "\"></trkpt>\n");



      } //for
        output.print(trackSeg);

        output.print("</trkseg>");
        output.print("</trk>");
        output.print("</gpx>");



      //Creates a FileReader Object
      //FileReader fr = new FileReader(file);


      output.close();
   } //write

}  //class

//_________________________________________________________________________________________________________________________________________________________________
//_________________________________________________________________________________________________________________________________________________________________
