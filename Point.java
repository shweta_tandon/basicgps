import static java.lang.Math.*;// importing neccessary libraries
import java.io.IOException;
import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;

//package cwk1;


/**
 * Representation of a point in space recorded by a GPS sensor.
 * AUTHOR: Shweta Tandon
 */
public class Point     //method to create a point in the track
{ //class

//BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
  // Constants useful for bounds checking

  private static final double MIN_LONGITUDE = -180.0; //declaring and initialisingh the values for maximum and minimum longitude and latitude
  private static final double MAX_LONGITUDE = 180.0; //declaring and initialisingh the values for maximum and minimum longitude and latitude
  private static final double MIN_LATITUDE = -90.0; //declaring and initialisingh the values for maximum and minimum longitude and latitude
  private static final double MAX_LATITUDE = 90.0; //declaring and initialisingh the values for maximum and minimum longitude and latitude
  private static final double MEAN_EARTH_RADIUS = 6.371009e+6; //declaring and initialisingh the values for maximum and minimum longitude and latitude

  // Fields of a point

   double longitude = 0;   // in degrees, West is negative
   double latitude = 0;    // in degrees
   double elevation = 0;   // above sea level, in metres

  //  constructor accepting a longitude & latitude
    Point(double newLongitude,double newLatitude) //default constructor which accepts values for longitude and latitude, in that order
    { //point
       longitude= newLatitude;
       latitude= newLongitude;


    } //point

  // create constructor accepting longitude, latitude & elevation
   Point(double newlongitude,double newlatitude,double newelevation) //default constructor which accepts values for longitude, latitude and elevation, in that orde
   { //point
     longitude=newlongitude;
     latitude=newlatitude;
     elevation=newelevation;

    if (longitude< MIN_LONGITUDE || longitude>MAX_LONGITUDE)
    { //if

    }//if
    if (longitude< MIN_LATITUDE|| longitude>MAX_LATITUDE)
    { //if

    } //if


  }//point

  // TO DO: create toString method
  //double toString(double[][] point= new double[0][2])
  public String toString() //method to print the latitude and longitudes
  { //toString
    return "(" + longitude + "," + latitude + "," + elevation + ")";
  }//toString
    /**
   * @return Longitude of this point, in degrees
   */
  public double getLongitude() //method to return the longitude of a point
  { //getLongitude
    return longitude; //returning longitude
  } //getLongitude

  /**
   * @return Latitude of this point, in degrees
   */
  public double getLatitude() //method to return the latitude of a point
  { //getLatitude
    return latitude; //returning latitude
  } //getLatitude

  /**
   * @return Elevation of this point above sea level, in metres
   */
  public double getElevation() //method to return the elevation of a point
  { //getElevation
    return elevation; //returning elevation
  } //getElevation

  public static double greatCircleDistance(Point i, Point j) //method to calculate the distance between two points in metres
  { //greatCircleDistance
    double phi1 = toRadians(i.getLatitude());
    double phi2 = toRadians(j.getLatitude());

    double lambda1 = toRadians(i.getLongitude());
    double lambda2 = toRadians(j.getLongitude());
    double delta = abs(lambda1 - lambda2);

    double firstTerm = cos(phi2)*sin(delta);
    double secondTerm = cos(phi1)*sin(phi2) - sin(phi1)*cos(phi2)*cos(delta);
    double top = sqrt(firstTerm*firstTerm + secondTerm*secondTerm);

    double bottom = sin(phi1)*sin(phi2) + cos(phi1)*cos(phi2)*cos(delta);

    return MEAN_EARTH_RADIUS * atan2(top, bottom);
  } //greatCircleDistance

}//class

//_________________________________________________________________________________________________________________________________________________________________
//_________________________________________________________________________________________________________________________________________________________________
